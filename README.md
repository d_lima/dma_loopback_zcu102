# dma_loopback

This project is a bare metal application to learn the DMA functionality with a basic loop back implementation for the zcu102 platform. The user can create the block design for Vivado using a .tcl file or follow the steps to create it.

## Getting Started

Only the source files are given:

- The dma_loopback.tcl file is used to create quickly the vivado project. Step by step process to create the block design is also included.
- Bare metal source code files to use with the sdk.

### Prerequisites

- Vivado 2018.1 to run the .tcl file.

### Generate the bitstream

#### Using the tcl

Open Vivado 2018.1 and create a new project for the zcu102.

Execute the .tcl:

```
source dma_loopback.tcl
```

Generate the bitstream, this may not take long time because the block diagram is very simple.

#### Step by step


### Create the bare metal application

Once the bitstream is generated, export hardware (including bitstream) and launch SDK.

Create a new empty application and with the deafult options named 'dma_test' and import the source files included in src/ directory (dma_test right click-import-general-file system).

Power on the board (ensure you have the JTAG and Serial cables connected) and right click dma_test files-debug as-debug configuration. 

On the left list, select last option (System debugger), double click, on target setup tab select 'Reset entire system'.

## Authors

* **David Lima** - *davidlimaastor@gmail.com* - [Bitbucket](https://bitbucket.org/d_lima)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments


