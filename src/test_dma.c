/* ----------------------------------------------------------------------------
 * ------------------------- Doom TFM (CEI 2017/2018)--------------------------
 * ----------------------------------------------------------------------------
 * 	DMA Test bare metal project
 * 		File: - "test_dma.c"
 *
 * ----------------------------------------------------------------------------
 * Author:  David Lima (davidlimaastor@gmail.com)
 *
 *
 * TODO:
 * 		- Check if there are more state indicators od the DMA state.
 *
 * CHANGELOG:
 *     [24-10-2018]: Hello world
 *     [25-10-2018]: Resets the dma.
 *     [07-11-2018]: Repo changes.
 *
 *---------------------------------------------------------------------------*/
#include "test_dma.h"


/*---------------------------------------------------------------------------*/
/*-
* This function prints info of the DMA current state.
*
* @param	DeviceId is the DMA device id.
*
* @return   None.
*
* @note		None.
*
*----------------------------------------------------------------------------*/
void DMAState()
{
	xil_printf(" DMA is busy direction dma to device: %u\r\n",
			XAxiDma_Busy(&AxiDma, XAXIDMA_DMA_TO_DEVICE));
	xil_printf(" DMA is busy direction device to dma: %u\r\n",
			XAxiDma_Busy(&AxiDma, XAXIDMA_DEVICE_TO_DMA));

}


/*---------------------------------------------------------------------------*/
/*-
* This function performance a reset of the DMA device and checks the device is
* coming out of reset or not.
*
* @param	DeviceId is the DMA device id.
*
* @return
*		- XST_SUCCESS if channel reset is successful
*		- XST_FAILURE if channel reset fails.
*
* @note		Xilinx example.
*
*----------------------------------------------------------------------------*/
int DMAReset(u16 DeviceId)
{
	xil_printf("-------------------------\r\n");
	xil_printf("Entering DMAReset \r\n");

	XAxiDma_Config *CfgPtr;
	int Status = XST_SUCCESS;

	/* Look up the hardware configuration */
	CfgPtr = XAxiDma_LookupConfig(DeviceId);
	if (!CfgPtr) {
		return XST_FAILURE;
	}

	/* Initializes the DMA engine */
	Status = XAxiDma_CfgInitialize(&AxiDma, CfgPtr);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	/* Perform a reset of the DMA device and checks the device is coming out */
	Status = XAxiDma_Selftest(&AxiDma);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	xil_printf("Checking DMAState ... \r\n");
	DMAState();
	xil_printf("Exiting DMAReset \r\n");


	return Status;
}


/*---------------------------------------------------------------------------*/
/*-
* This function performance the data transference between the DMA and the
* stretch4x module.
*
* @param	DeviceId is the Stretch4x_hw device id.
*
* @return
*		- XST_SUCCESS if successful
*		- XST_FAILURE if fails.
*
* @note		None.
*
*----------------------------------------------------------------------------*/
int DMATransfer(u16 DeviceID)
{
   	xil_printf("--------------------------------\r\n");
   	xil_printf("Entering DMATransfer ... \r\n");

	int Status = XST_SUCCESS;
	u8 *TxBufferPtr;

	u8 value;

	TxBufferPtr = (u8*)TX_BUFFER_BASE;

	/* Disable interrupts, we use polling mode */
	XAxiDma_IntrDisable(&AxiDma, XAXIDMA_IRQ_ALL_MASK, XAXIDMA_DMA_TO_DEVICE);

	for(int i = 0; i < 10;  i++) {

		// Dummy value
		value = i;

		xil_printf("Sent value i: %u, value: %x\r\n", i, value);

		TxBufferPtr[i] = value;

	}

	/* Flush the SrcBuffer before the DMA transfer, in case the Data Cache
	 * is enabled
	 */
	Xil_DCacheFlushRange((UINTPTR)TxBufferPtr, 10);

	/* Simple transfer Tx */
	Status = XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) TxBufferPtr,
			10, XAXIDMA_DMA_TO_DEVICE);

	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	xil_printf("Checking DMAState ... \r\n");
	DMAState();

	while(XAxiDma_Busy(&AxiDma,XAXIDMA_DMA_TO_DEVICE)) {
		// Wait
	}

   	return Status;
}


/*---------------------------------------------------------------------------*/
/*-
* This function read the data from DMA.
*
* @param	DeviceId is the Stretch4x_hw device id.
*
* @return
*		- XST_SUCCESS if successful
*		- XST_FAILURE if fails.
*
* @note		None.
*
*----------------------------------------------------------------------------*/
int DMAReceive(u16 DeviceID)
{

   	xil_printf("--------------------------------\r\n");
   	xil_printf("Entering DMAReceive function ... \r\n");


	int Status = XST_SUCCESS;

	u8 *RxBufferPtr;

	u8 value;

	RxBufferPtr = (u8*)RX_BUFFER_BASE;

   	Xil_DCacheFlushRange((UINTPTR)RxBufferPtr, 10);

	/* Simple transfer Rx */
	Status = XAxiDma_SimpleTransfer(&AxiDma,(UINTPTR) RxBufferPtr,
			10, XAXIDMA_DEVICE_TO_DMA);

	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	xil_printf("Checking DMAState ... \r\n");
	DMAState();

	while(XAxiDma_Busy(&AxiDma,XAXIDMA_DEVICE_TO_DMA)) {
		// Wait
	}

   	for (int i=0; i<10; i++) {

		value = RxBufferPtr[i];

		xil_printf("Received i: %u, DataT: %x\r\n",i ,value);
	}

   	return Status;
}


/*---------------------------------------------------------------------------*/
int main()
{
   	xil_printf("--------------------------------\r\n");
   	xil_printf("Entering main ... \r\n");

    init_platform();

    int Status;

   	/*------------- Run the DMAReset function to reset the DMA --------------*/
   	Status = DMAReset(DMA_DEV_ID);

   	if (Status != XST_SUCCESS) {
   		xil_printf("DMAReset Failed\r\n");
  		return XST_FAILURE;
   	}


   	/*---------------------- Send data to module ----------------------------*/
   	Status = DMATransfer(DMA_DEV_ID);

   	if (Status != XST_SUCCESS) {
		xil_printf("DMATrasnfer Failed\r\n");
		return XST_FAILURE;
   	}

   	/*------------------------ Receive data ---------------------------------*/
   	Status = DMAReceive(DMA_DEV_ID);

   	if (Status != XST_SUCCESS) {
		xil_printf("DMATrasnfer Failed\r\n");
		return XST_FAILURE;
   	}

   	xil_printf("Successfully ran dma_test Example\r\n");
   	xil_printf("Exiting main ... \r\n");

    cleanup_platform();

   	return XST_SUCCESS;
}
