/* ----------------------------------------------------------------------------
 * ------------------------- Doom TFM (CEI 2017/2018)--------------------------
 * ----------------------------------------------------------------------------
 * 	DMA Test bare metal project
 * 		File: - "test_dma.h"
 *
 * ----------------------------------------------------------------------------
 * Author:  David Lima (davidlimaastor@gmail.com)
 *
 *
 * TODO: See .c file.
 *
 * CHANGELOG:
 *     [24-10-2018]: Hello world
 *     [07-11-2018]: Repo changes.
 *
 *---------------------------------------------------------------------------*/
#ifndef SRC_TEST_DMA_H_
#define SRC_TEST_DMA_H_

/*---------------------------- Included Files -------------------------------*/
#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"

#include "xaxidma.h"
#include "xparameters.h"
#include "xdebug.h"


/*------------------------- Constant Definitions ----------------------------*/
#define DMA_DEV_ID				XPAR_AXIDMA_0_DEVICE_ID

/*
 * Device hardware build related constants.
 */

#define MEM_BASE_ADDR		0x01000000

#define TX_BUFFER_BASE		(MEM_BASE_ADDR + 0x00100000)
#define RX_BUFFER_BASE		(MEM_BASE_ADDR + 0x00300000)
#define RX_BUFFER_HIGH		(MEM_BASE_ADDR + 0x004FFFFF)


/*------------------------- Variable Definitions ----------------------------*/
/*
 * Device instance definitions
 */
XAxiDma AxiDma;

/*------------------------- Function Prototypes -----------------------------*/
int DMAReset(u16 DeviceId);
int DMATransfer(u16 DeviceId);
int DMAReceive(u16 DeviceId);


#endif /* SRC_TEST_DMA_H_ */
